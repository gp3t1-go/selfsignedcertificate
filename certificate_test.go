package selfsignedcertificate

import (
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"testing"
	"time"
)

var (
	startingPort = 22222

	tests = []TCert{
		{
			name:    "test1",
			path:    "./test_data/cert1",
			keySize: 2048,
			template: NewX509Template(
				pkix.Name{
					CommonName:   "toto",
					Country:      []string{"FR"},
					Organization: []string{"X"}},
				10,
				x509.KeyUsageDigitalSignature|x509.KeyUsageCertSign,
				[]x509.ExtKeyUsage{
					x509.ExtKeyUsageClientAuth,
					x509.ExtKeyUsageServerAuth}),
			addr: NewAddress()},
		{
			name:    "test2",
			path:    "./test_data/cert2",
			keySize: 4096,
			template: NewX509Template(
				pkix.Name{
					CommonName:   "toto",
					Country:      []string{"FR"},
					Organization: []string{"X"},
					Locality:     []string{"PARIS"},
					PostalCode:   []string{"75001"}},
				10,
				x509.KeyUsageDigitalSignature|x509.KeyUsageCertSign,
				[]x509.ExtKeyUsage{
					x509.ExtKeyUsageClientAuth,
					x509.ExtKeyUsageServerAuth}),
			addr: NewAddress()},
	}
)

type TCert struct {
	name     string
	path     string
	keySize  int
	template *x509.Certificate
	addr     string
	srv      *http.Server
}

func (t *TCert) getHTTPServer() *http.Server {
	t.srv = &http.Server{
		Addr:    t.addr,
		Handler: &THandler{},
		TLSConfig: &tls.Config{
			MinVersion:               tls.VersionTLS12,
			CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
			PreferServerCipherSuites: true,
			CipherSuites: []uint16{
				tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
				tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_RSA_WITH_AES_256_CBC_SHA,
			},
		},
	}
	return t.srv
}

type THandler struct{}

func (t *THandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	body := "NO REQUEST BODY"
	if b, err := ioutil.ReadAll(r.Body); err == nil {
		body = string(b)
	}
	w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(fmt.Sprintf("SERVER OK(%s)", body)))
}

func NewAddress() string {
	a := fmt.Sprintf("127.0.0.1:%d", startingPort)
	startingPort++
	return a
}

func checkHTTPServer(addr string) error {
	// HTTPS CLIENT
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	resp, errGet := client.Get(fmt.Sprintf("https://%s/", addr))
	if errGet != nil {
		return errGet
	}

	defer resp.Body.Close()
	body, errBody := ioutil.ReadAll(resp.Body)
	if errBody != nil {
		//t.Errorf("[HTTPS CLIENT ERROR] No Response Body: %s", errBody)
		return errBody
	}
	if string(body) != "SERVER OK()" {
		return fmt.Errorf("[HTTPS CLIENT ERROR] Bad response: '%s'", string(body))
	}
	return nil
}

func TestGenCertificate(t *testing.T) {
	for _, tCert := range tests {
		tCert := tCert
		t.Run(tCert.name, func(t *testing.T) {
			t.Parallel()

			t.Logf("\t[%s] starting test in %s with keysize=%d", tCert.name, tCert.path, tCert.keySize)
			keyFilename := tCert.name + ".key.pem"
			certFilename := tCert.name + ".cert.pem"

			// Generate Keys
			privKey, err := GenRSAKey(tCert.keySize)
			if err != nil {
				t.Errorf("[RSAKEY ERROR] Cannot generate keys \n\tdata: %+v\n\terror: %s", tCert, err)
				return
			}
			t.Logf("\t[%s] RSA key generated with key size=%d", tCert.name, tCert.keySize)

			// Write Keys to disk
			err = WritePEMKey(privKey, tCert.path, keyFilename)
			if err != nil {
				t.Errorf("[RSAKEY ERROR] Cannot write keys to disk \n\tdata: %+v\n\terror: %s", tCert, err)
				return
			}
			t.Logf("\t[%s] RSA key written to %s", tCert.name, tCert.path)

			// Generate Certificate
			cert, err := GenCertificate(privKey, tCert.template)
			if err != nil {
				t.Errorf("[CERTIFICATE ERROR] Cannot generated certificate \n\tdata: %+v\n\terror: %s", tCert, err)
				return
			}
			t.Logf("\t[%s] Certificate generated", tCert.name)

			// Write Certificate to disk
			err = WritePEMCertificate(cert, tCert.path, certFilename)
			if err != nil {
				t.Errorf("[CERTIFICATE ERROR] Cannot write certificate to disk \n\tdata: %+v\n\terror: %s", tCert, err)
				return
			}
			t.Logf("\t[%s] Certificate written to %s", tCert.name, tCert.path)

			// HTTPS SERVER

			server := tCert.getHTTPServer()

			go func() {
				t.Logf("\t[%s] HTTPS Server starting on %s with certificate %s and key %s", tCert.name, tCert.addr, filepath.Join(tCert.path, certFilename), filepath.Join(tCert.path, keyFilename))
				err := server.ListenAndServeTLS(filepath.Join(tCert.path, certFilename), filepath.Join(tCert.path, keyFilename))
				if err != nil && fmt.Sprintf("%s", err) != "http: Server closed" {
					t.Errorf("[HTTPS SERVER ERROR] Cannot start server: %s", err)
				}
				t.Logf("\t[%s] HTTPS Server stopped: %s", tCert.name, err)
			}()

			time.Sleep(1 * time.Second)
			errCheck := checkHTTPServer(tCert.addr)
			if errCheck != nil {
				t.Errorf("[HTTPS CLIENT ERROR] %s", errCheck)
			} else {
				t.Logf("\t[%s] client get proper response", tCert.name)
			}
			if server != nil {
				server.Close()
				time.Sleep(1 * time.Second)
			}
		})
	}
}
