package selfsignedcertificate

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	mathrand "math/rand"
	"os"
	"path/filepath"
	"time"
)

func NewX509Template(subject pkix.Name, days int, keyUsage x509.KeyUsage, extKeyUsage []x509.ExtKeyUsage) *x509.Certificate {
	now := time.Now()
	mathrand.Seed(now.UTC().UnixNano())

	subjectKeyId := make([]byte, 4)
	rand.Read(subjectKeyId)

	serialNumber := big.NewInt(mathrand.Int63())

	template := &x509.Certificate{
		IsCA: true,
		BasicConstraintsValid: true,
		SubjectKeyId:          subjectKeyId,
		SerialNumber:          serialNumber,
		Subject:               subject,
		NotBefore:             now,
		NotAfter:              now.AddDate(0, 0, days),
		// see http://golang.org/pkg/crypto/x509/#KeyUsage
		ExtKeyUsage: extKeyUsage,
		KeyUsage:    keyUsage,
	}

	return template
}

func GenRSAKey(keySize int) (*rsa.PrivateKey, error) {
	privKey, err := rsa.GenerateKey(rand.Reader, keySize)
	if err != nil {
		return nil, fmt.Errorf("unable generate a new rsa private key(%s)", err)
	}
	return privKey, nil
}

func GenCertificate(privateKey *rsa.PrivateKey, template *x509.Certificate) ([]byte, error) {
	cert, err := x509.CreateCertificate(rand.Reader, template, template, &privateKey.PublicKey, privateKey)
	if err != nil {
		return nil, fmt.Errorf("unable generate a new certificate(%s)", err)
	}
	return cert, nil
}

func ensurePathExists(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		if err = os.MkdirAll(path, 0700); err != nil {
			return err
		}
	}
	return nil
}

func WritePEMCertificate(cert []byte, path string, filename string) error {
	if errDir := ensurePathExists(path); errDir != nil {
		return errDir
	}
	fName := filepath.Join(path, filename)

	certOut, err := os.OpenFile(fName, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	defer certOut.Close()
	if err != nil {
		return fmt.Errorf("failed to open %s for writing: %s", fName, err)
	}
	err = pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: cert})
	if err != nil {
		return fmt.Errorf("failed to write certificate to %s: %s", fName, err)
	}
	return nil
}

func WritePEMKey(privateKey *rsa.PrivateKey, path string, filename string) error {
	if errDir := ensurePathExists(path); errDir != nil {
		return errDir
	}
	fName := filepath.Join(path, filename)

	keyOut, err := os.OpenFile(fName, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	defer keyOut.Close()
	if err != nil {
		return fmt.Errorf("failed to open %s for writing: %s", fName, err)
	}
	err = pem.Encode(keyOut, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privateKey)})
	if err != nil {
		return fmt.Errorf("failed to write key in pem format to %s: %s", fName, err)
	}
	return nil
}
